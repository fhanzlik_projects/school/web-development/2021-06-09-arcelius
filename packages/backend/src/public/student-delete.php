<?php

require_once "../config.php";

$db_conn = open_db_conn();

$variables = msgpack_unpack(file_get_contents('php://input'));
$id = $variables["id"];

$smt = $db_conn->prepare("delete from students where id = ?");
$smt->bind_param("i", $id);
$smt->execute();
if ($res = $smt->get_result() === false) {
	echo msgpack_pack((object) []);
} else {
	throw new Error($smt->error);
}
