<?php

require_once "../config.php";

$db_conn = open_db_conn();

$variables = msgpack_unpack(file_get_contents('php://input'));
$query = $variables["query"];

$res = null;
if (strlen($query) === 0) {
	$res = $db_conn->query("select id, name_first, name_last from main.students");
} else {
	$smt = $db_conn->prepare("SELECT id, name_first, name_last from main.students where `name_first` like concat( '%', ?, '%' ) or `name_last` like concat( '%', ?, '%' )");
	$smt->bind_param("ss", $query, $query);
	$smt->execute();
	$res = $smt->get_result();
}


echo msgpack_pack($res->fetch_all(MYSQLI_ASSOC));
