module.exports = {
	env: {
		browser: true,
		es2021: true,
		node: true,
	},
	extends: [
		"eslint:recommended",
		"plugin:react/recommended",
		"plugin:react-hooks/recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:prettier/recommended",
	],
	parser: "@typescript-eslint/parser",
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: 12,
		sourceType: "module",
	},
	settings: {
		react: { version: "detect" },
	},
	plugins: ["react", "@typescript-eslint", "prettier"],
	rules: {
		"prettier/prettier": "error",
		// this would be too much work with (IMHO) not high enough gain
		"@typescript-eslint/explicit-module-boundary-types": "off",
		"react/self-closing-comp": "error",
		"react/button-has-type": "error",
		// NextJS adds the import in the transpilation phase automatically
		"react/react-in-jsx-scope": "off",
		"react/display-name": "error",
	},
};
