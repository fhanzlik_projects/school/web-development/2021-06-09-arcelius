import { css, SerializedStyles } from "@emotion/react";
import { Theme } from "./theme";

export type Interpolatable = (props: { theme: Theme }) => SerializedStyles;

export const link = css`
	cursor: pointer;
`;
