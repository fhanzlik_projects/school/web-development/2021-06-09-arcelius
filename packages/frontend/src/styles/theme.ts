import { css, Theme as EmotionTheme } from "@emotion/react";
import { Interpolatable as Interpolatable } from "./common";

export type Theme = EmotionTheme;

const palette = {
	black: "hsl(0, 0%, 0%)",
	black_dimmed: "hsl(0, 0%, 30%)",
	white: "hsl(0, 0%, 100%)",
	white_dimmed: "hsl(0, 0%, 95%)",
	grey: "hsl(0, 0%, 90%)",
	blue: "#9500ff",
};
export const themes: Record<"light", Theme> = {
	light: {
		color: {
			foreground: {
				primary: palette.black_dimmed,
				accent: palette.white_dimmed,
			},
			background: {
				primary: palette.white,
				primary_dimmed: palette.white_dimmed,
				accent: palette.blue,
			},
		},
	},
};

export const background =
	(name: keyof Theme["color"]["background"]): Interpolatable =>
	({ theme }) =>
		css`
			background-color: ${theme.color.background[name]};
		`;

export const shadow = (): Interpolatable => () =>
	css`
		box-shadow: 0px 0px 8px 2px rgba(0, 0, 0, 0.25);
	`;
