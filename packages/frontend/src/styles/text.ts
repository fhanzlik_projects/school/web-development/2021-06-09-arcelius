import { css } from "@emotion/react";
import { Theme } from "./theme";

const families = {
	normal: "'Roboto', sans-serif",
	input: "'Montserrat', sans-serif",
	// normal: 'enervate',
	// bold: 'enervate',
};

const font = (family: keyof typeof families, size: string, weight = 400) => css`
	font-family: ${families[family]};
	font-size: ${size};
	font-weight: ${weight};
`;

const bases = {
	small: font("normal", "0.75rem"),
	normal: font("normal", "1rem"),
	normal_bold: font("normal", "1rem", 700),
	input: font("input", "1rem"),
	emoji_icon: font("normal", "2rem"),
	emoji_icon_small: font("normal", "1.5rem"),
};

export const text =
	(
		base: keyof typeof bases,
		{
			color = "primary",
		}: {
			color?: keyof Theme["color"]["foreground"];
		} = {},
	) =>
	({ theme }: { theme: Theme }) =>
		css`
			text-decoration: none;
			${bases[base]}

			color: ${theme.color.foreground[color]};
		`;
