import { css } from "@emotion/react";

export const globalReset = css`
	/* very brave reset */
	* {
		vertical-align: baseline;
		font-weight: inherit;
		font-family: inherit;
		font-style: inherit;
		font-size: 100%;
		border: 0 none;
		padding: 0;
		margin: 0;
		box-sizing: border-box;
		background: none;

		/* attempt to force people to correctly apply text styles */
		color: #f0f;
	}
`;

export const reset = {
	ul: css`
		list-style: none;
		padding: 0;
	`,
};
