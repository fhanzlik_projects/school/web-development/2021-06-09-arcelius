import "@emotion/react";

declare module "@emotion/react" {
	export interface Theme {
		color: {
			foreground: {
				primary: string;
				accent: string;
			};
			background: {
				primary: string;
				primary_dimmed: string;
				accent: string;
			};
		};
	}
}
