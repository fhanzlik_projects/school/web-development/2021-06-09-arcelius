export const error = (message: string) => {
	throw new Error(message);
};

export const assertionFail = (message: string) => {
	throw new Error(`assertion failed: ${message}`);
};
