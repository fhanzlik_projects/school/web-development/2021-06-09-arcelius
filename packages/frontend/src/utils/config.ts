import { error } from "./exceptions";

export const backendUrl =
	process.env["NEXT_PUBLIC_ARCELIUS_BE_URL"] ??
	error("backend url not provided in `NEXT_PUBLIC_ARCELIUS_BE_URL`");
