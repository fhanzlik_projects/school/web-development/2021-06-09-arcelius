import {
	TypedUseSelectorHook,
	useDispatch as useReduxDispatch,
	useSelector as useReduxSelector,
} from "react-redux";
import { RootDispatch, RootState } from "../redux";

export const useDispatch = () => useReduxDispatch<RootDispatch>();
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;
