export const findOrUndefined = <T>(value: unknown, array: readonly T[]) =>
	array.find((item) => item === value);
