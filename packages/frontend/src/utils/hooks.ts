import _ from "lodash";
import { useCallback, useMemo, useState } from "react";

export const useDebouncedPublishState = <T>(
	initialValue: T,
	submit: (value: T) => void,
) => {
	const [value, setValue] = useState(initialValue);

	const submitDebounced = useMemo(() => _.debounce(submit, 500), [submit]);
	const set = useCallback(
		(value: T) => {
			setValue(value);
			submitDebounced(value);
		},
		[submitDebounced],
	);

	return [value, set] as const;
};
