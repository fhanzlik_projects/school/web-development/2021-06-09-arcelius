const locales = [
	{ code: "en-GB", name: "English (GB)", emoji: "🇬🇧" },
	{ code: "cs-CZ", name: "Čeština", emoji: "🇨🇿" },
];

module.exports = {
	locales,
	defaultLocale:
		locales[0] ??
		(() => {
			throw new Error("assertion failed: fallback locale does not exist");
		})(),
};
