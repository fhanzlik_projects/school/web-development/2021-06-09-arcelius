import { css } from "@emotion/react";
import styled from "@emotion/styled";

export const Space = styled.div<{ width?: string; height?: string }>(
	({ width, height }) => css`
		display: inline-block;
		width: ${width ?? "100%"};
		height: ${height ?? "100%"};
	`,
);
