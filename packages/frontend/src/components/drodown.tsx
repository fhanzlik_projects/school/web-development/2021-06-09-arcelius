import { css } from "@emotion/react";
import styled from "@emotion/styled";
import React, {
	ReactNode,
	useCallback,
	useEffect,
	useRef,
	useState,
} from "react";
import { shadow } from "../styles/theme";
import { Button } from "./forms/button";

export const Dropdown = ({
	renderToggle,
	children,
}: {
	renderToggle: () => ReactNode;
	children: ReactNode;
}) => {
	const [isOpen, setIsOpen] = useState(false);
	const containerRef = useRef<HTMLDivElement>(null);
	const handleClickInDocument = useCallback((e: MouseEvent) => {
		if (
			e.target instanceof Node &&
			!containerRef.current?.contains(e.target)
		) {
			setIsOpen(false);
		}
	}, []);

	useEffect(() => {
		document.addEventListener("mousedown", handleClickInDocument);

		return () => {
			document.removeEventListener("mousedown", handleClickInDocument);
		};
	}, [handleClickInDocument]);

	return (
		<div ref={containerRef}>
			<Button onPress={() => setIsOpen(!isOpen)}>{renderToggle()}</Button>
			<Content visible={isOpen}>{children}</Content>
		</div>
	);
};

const Content = styled.div<{ visible: boolean }>`
	position: absolute;
	/* since some other elements on the page might need non-static positioning, this will is needed to assure that the dropdown stays above normal content */
	z-index: 1000;
	top: calc(100% + 1rem);
	right: 1rem;
	width: 12rem;

	background-color: white;
	${shadow()}
	transition: opacity 0.1s, visibility 0.1s, transform 0.1s;

	${({ visible }) =>
		visible
			? css`
					visibility: visible;
					opacity: 1;
					transform: translateY(0);
			  `
			: css`
					visibility: hidden;
					opacity: 0;
					transform: translateY(-1rem);
			  `};
`;
