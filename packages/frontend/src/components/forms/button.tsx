import styled from "@emotion/styled";
import { ReactNode } from "react";

export const Button = ({
	onPress,
	children,
}: {
	onPress: () => void;
	children: ReactNode;
}) => (
	<SButton onClick={onPress} type="button">
		{children}
	</SButton>
);
const SButton = styled.button`
	cursor: pointer;
	opacity: 1;

	transition: 0.5s opacity;
	:active {
		opacity: 0.5;
	}
`;
