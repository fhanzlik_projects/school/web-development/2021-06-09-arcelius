import styled from "@emotion/styled";
import React, { useCallback } from "react";
import { text } from "../../styles/text";

export const TextInput = ({
	setValue,
	value,
}: {
	value: string;
	setValue: (value: string) => void;
}) => (
	<Field
		value={value}
		onChange={useCallback((e) => setValue(e.target.value), [setValue])}
	/>
);

const Field = styled.input`
	width: 16rem;
	border-radius: 0.3rem;
	padding: 0.25rem;
	${text("input")}
	border: 0.1rem solid ${({ theme }) => theme.color.foreground.primary};
	:active {
		border: 0.1rem solid ${({ theme }) => theme.color.background.accent};
	}
`;
