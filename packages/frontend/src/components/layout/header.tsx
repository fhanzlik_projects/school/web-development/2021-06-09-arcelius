import styled from "@emotion/styled";
import React, { ReactNode } from "react";
import { shadow } from "../../styles/theme";
import { LanguageSwitcher } from "../language-switcher";

export const Header = ({ children }: { children: ReactNode }) => (
	<Container>
		<AdditionalContent>{children}</AdditionalContent>
		<LanguageSwitcher />
	</Container>
);

const Container = styled.div`
	position: relative;
	height: 4rem;

	display: flex;
	justify-content: space-between;
	align-items: center;

	background-color: white;
	${shadow()}
`;

const AdditionalContent = styled.div`
	display: flex;

	align-items: stretch;
	padding: 0.5rem 1.5rem;
`;
