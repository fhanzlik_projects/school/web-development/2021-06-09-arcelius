import { Global } from "@emotion/react";
import styled from "@emotion/styled";
import React, { ReactNode } from "react";
import { globalReset } from "../../styles/reset";
import { text } from "../../styles/text";
import { Header } from "./header";

export const Layout = ({
	children,
	headerContent,
}: {
	children: ReactNode;
	headerContent?: ReactNode;
}) => (
	<>
		<Global styles={globalReset} />
		<Container>
			<Header>{headerContent}</Header>
			<Content>{children}</Content>
		</Container>
	</>
);

const Container = styled.div`
	min-height: 100vh;

	background-color: ${({ theme }) => theme.color.background.primary};
	${text("normal")}
`;

const Content = styled.div`
	padding: 5rem 15vw;
`;
