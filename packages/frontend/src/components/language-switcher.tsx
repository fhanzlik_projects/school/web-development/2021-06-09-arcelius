import styled from "@emotion/styled";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { link } from "../styles/common";
import { reset } from "../styles/reset";
import { text } from "../styles/text";
import { defaultLocale, locales } from "../utils/locales";
import { Dropdown } from "./drodown";
import { Space } from "./space";

export const LanguageSwitcher = () => {
	const { route } = useRouter();
	const { locale: activeLocale } = useLocaleInfo();

	return (
		<Dropdown
			renderToggle={() => <ToggleIcon>{activeLocale.emoji}</ToggleIcon>}
		>
			<List>
				{locales.map((locale) => (
					<Item key={locale.code}>
						<Link href={route} locale={locale.code}>
							<Anchor>
								<IconSmall>{locale.emoji}</IconSmall>
								<Space width="1rem" />
								{locale.name}
							</Anchor>
						</Link>
					</Item>
				))}
			</List>
		</Dropdown>
	);
};

const useLocaleInfo = () => {
	const { locale: code } = useRouter();
	if (code === undefined) throw new Error();

	return {
		locale: locales.find((locale) => locale.code === code) ?? defaultLocale,
	};
};

const ToggleIcon = styled.span`
	display: block;
	width: 4rem;
	height: 4rem;
	display: flex;
	align-items: center;
	justify-content: center;
	${text("emoji_icon")}
`;
const IconSmall = styled.span`
	${text("emoji_icon_small")}
`;

const List = styled.ul`
	${reset.ul}
	display: flex;
	flex-direction: column;
	padding: 1rem 1.25rem;
	gap: 1.5rem;
`;

const Item = styled.li`
	padding: 0;
`;

const Anchor = styled.a`
	${text("normal")}
	${link}
	display: flex;
	align-items: center;
`;
