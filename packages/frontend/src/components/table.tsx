import { keyframes } from "@emotion/react";
import styled from "@emotion/styled";
import React from "react";
import { useSortBy, useTable } from "react-table";
import { text } from "../styles/text";
import { background } from "../styles/theme";
import { Button } from "./forms/button";

const fadeIn = keyframes`
	from {
		opacity: 0;
  	}
  	to {
		opacity: 1;
  	}
`;

export const STable = styled.table`
	table-layout: fixed;
	border-collapse: collapse;
	width: 100%;
	overflow-x: scroll;
`;

export const Cell = styled.td`
	position: relative;

	${text("normal")}
	padding: 1rem;

	// sorry, I have spent far too long trying to add a scrollbar, but to no avail
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
`;
export const HCell = styled(Cell.withComponent("th"))`
	${text("normal_bold", { color: "accent" })};
	text-align: left;
`;
const ActionCell = styled(Cell)`
	width: 3rem;
	vertical-align: middle;
	padding: 0.5rem;

	${text("emoji_icon_small")}
`;

export const Row = styled.tr`
	animation: ${fadeIn} 1s ease;

	&:nth-of-type(2n) {
		${background("primary_dimmed")}
	}

	${ActionCell} {
		opacity: 0;
		transition: 0.25s opacity;
	}
	:hover ${ActionCell} {
		opacity: 1;
	}
`;
export const HRow = styled(Row)`
	${background("accent")}
`;

const HActionCell = ActionCell.withComponent("th");

export const Table = <
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	TKey extends keyof any,
	TItem extends Record<TKey, string | number>,
>({
	data,
	columns,
	deleteItem,
}: {
	data: TItem[];
	columns: { Header: string; accessor: TKey }[];
	deleteItem: (data: TItem) => void;
}) => {
	const { getTableProps, headerGroups, getTableBodyProps, rows, prepareRow } =
		useTable(
			{
				columns,
				data,
			},
			useSortBy,
		);

	return (
		<STable {...getTableProps()}>
			<thead>
				{headerGroups.map((headerGroup) => (
					// eslint-disable-next-line react/jsx-key
					<HRow {...headerGroup.getHeaderGroupProps()}>
						{headerGroup.headers.map((column) => (
							// eslint-disable-next-line react/jsx-key
							<HCell
								{...column.getHeaderProps(
									column.getSortByToggleProps(),
								)}
							>
								{column.render("Header")}
								{column.isSorted
									? column.isSortedDesc
										? " 🔽"
										: " 🔼"
									: ""}
							</HCell>
						))}
						<HActionCell />
					</HRow>
				))}
			</thead>

			<tbody {...getTableBodyProps()}>
				{rows.map((row) => {
					prepareRow(row);

					return (
						// eslint-disable-next-line react/jsx-key
						<Row {...row.getRowProps()}>
							{row.cells.map((cell) => (
								// eslint-disable-next-line react/jsx-key
								<Cell {...cell.getCellProps()}>
									{cell.render("Cell")}
								</Cell>
							))}
							<ActionCell>
								<Button
									onPress={() => {
										deleteItem(row.original);
									}}
								>
									❌
								</Button>
							</ActionCell>
						</Row>
					);
				})}
			</tbody>
		</STable>
	);
};
