import { configureStore } from "@reduxjs/toolkit";
import { cacheSlice } from "./cache";
import { themeSlice } from "./theme";

export const reduxStore = configureStore({
	reducer: {
		theme: themeSlice.reducer,
		cache: cacheSlice.reducer,
	},
});
export type RootState = ReturnType<typeof reduxStore.getState>;
export type RootDispatch = typeof reduxStore.dispatch;
