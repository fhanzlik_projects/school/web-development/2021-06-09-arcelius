import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { themes } from "../styles/theme";

export const themeSlice = createSlice({
	name: "theme",
	initialState: themes.light,
	reducers: {
		set: (_, action: PayloadAction<keyof typeof themes>) =>
			themes[action.payload],
	},
});
