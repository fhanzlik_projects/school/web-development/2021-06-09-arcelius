import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface Student {
	id: number;
	name_first: string;
	name_last: string;
}
export interface Cache {
	students: Student[];
}

const initialState: Cache = {
	students: [],
};

export const cacheSlice = createSlice({
	name: "cache",
	initialState,
	reducers: {
		set: (_, action: PayloadAction<Cache>) => action.payload,
	},
});
