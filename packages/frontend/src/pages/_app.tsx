import { ThemeProvider as EmotionThemeProvider } from "@emotion/react";
import { i18n } from "@lingui/core";
import { I18nProvider } from "@lingui/react";
import { cs, en } from "make-plural/plurals";
import { AppProps } from "next/dist/next-server/lib/router/router";
import { useRouter } from "next/router";
import React, { ReactNode, useEffect } from "react";
import { Provider as ReduxProvider } from "react-redux";
import { reduxStore } from "../redux";
import { useSelector } from "../utils/redux hooks";

i18n.loadLocaleData("en-GB", { plurals: en });
i18n.loadLocaleData("cs-CZ", { plurals: cs });

const Page = ({ Component, pageProps }: AppProps) => {
	const { locale } = useRouter();

	useEffect(() => {
		if (locale === undefined) return;

		(async () => {
			const { messages } = await import(
				`../../locales/${locale}/messages.po`
			);

			i18n.load(locale, messages);
			i18n.activate(locale);
		})();
	}, [locale]);

	return (
		<Providers>
			<Component {...pageProps} />
		</Providers>
	);
};
export default Page;

const Providers = ({ children }: { children: ReactNode }) => {
	return (
		<ReduxProvider store={reduxStore}>
			<I18nProvider i18n={i18n}>
				<ThemeProvider> {children}</ThemeProvider>
			</I18nProvider>
		</ReduxProvider>
	);
};

export const ThemeProvider = ({ children }: { children: ReactNode }) => {
	const theme = useSelector((state) => state.theme);

	return (
		<EmotionThemeProvider theme={theme}>{children}</EmotionThemeProvider>
	);
};
