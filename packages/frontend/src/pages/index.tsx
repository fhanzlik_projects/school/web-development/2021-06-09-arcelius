import styled from "@emotion/styled";
import { t } from "@lingui/macro";
import { pack, unpack } from "msgpackr";
import React, { useCallback, useEffect, useMemo } from "react";
import { TextInput } from "../components/forms/text input";
import { Layout } from "../components/layout";
import { Table } from "../components/table";
import { reduxStore } from "../redux";
import { Cache, cacheSlice, Student } from "../redux/cache";
import { shadow } from "../styles/theme";
import { backendUrl } from "../utils/config";
import { useDebouncedPublishState } from "../utils/hooks";
import { useDispatch, useSelector } from "../utils/redux hooks";

const Index = () => {
	const columns = useMemo(
		() => [
			{ Header: t`id`, accessor: "id" as const },
			{ Header: t`name.first`, accessor: "name_first" as const },
			{ Header: t`name.last`, accessor: "name_last" as const },
		],
		[],
	);

	const { data: students, refetch: refetchStudents } = useStudentData(
		useMemo(() => ({ variables: { query: "" } }), []),
	);
	const deleteStudent = useStudentDelete();
	const [searchQuery, setSearchQuery] = useDebouncedPublishState<string>(
		"",
		useCallback(
			(query) => refetchStudents({ variables: { query } }),
			[refetchStudents],
		),
	);

	if (students === undefined) return null;

	return (
		<Layout
			headerContent={
				<TextInput value={searchQuery} setValue={setSearchQuery} />
			}
		>
			<Box>
				<Table
					columns={columns}
					data={students}
					deleteItem={({ id }) =>
						deleteStudent({ variables: { id } })
					}
				/>
			</Box>
		</Layout>
	);
};
export default Index;

const Box = styled.div`
	${shadow()}
	border-radius: 0.5rem;
	overflow: hidden;
`;

const request = async <TVariables, TResult>(
	route: string,
	{ method, variables }: { method: string; variables: TVariables },
): Promise<TResult> =>
	unpack(
		new Uint8Array(
			await (
				await fetch(`${backendUrl}/${route}`, {
					method,
					body: pack(variables),
				})
			).arrayBuffer(),
		),
	);

const useRequest = <TVariables, TResponse>({
	send,
	update,
}: {
	send: (variables: TVariables) => Promise<TResponse>;
	update: (params: {
		response: TResponse;
		cache: Cache;
		variables: TVariables;
	}) => Cache;
}): ((props: { variables: TVariables }) => Promise<TResponse>) => {
	const dispatch = useDispatch();

	return useCallback(
		async ({ variables }) => {
			const response = await send(variables);

			dispatch(
				cacheSlice.actions.set(
					update({
						response,
						cache: reduxStore.getState().cache,
						variables,
					}),
				),
			);

			return response;
		},
		[dispatch, send, update],
	);
};

interface StudentQueryVariables {
	query: string;
}
const useStudentData = (initialParams: {
	variables: StudentQueryVariables;
}) => {
	const data = useSelector((state) => state.cache.students);
	const dispatch = useDispatch();

	const fetch = useRequest<StudentQueryVariables, Student[]>({
		send: useCallback(
			(variables) =>
				request("students.php", { method: "POST", variables }),
			[],
		),
		update: useCallback(
			({ response, cache }) => ({ ...cache, students: response }),
			[],
		),
	});
	const fetchAndSet = useCallback(
		async (params: { variables: StudentQueryVariables }) =>
			dispatch(
				cacheSlice.actions.set({
					...reduxStore.getState(),
					students: await fetch(params),
				}),
			),
		[dispatch, fetch],
	);

	useEffect(() => {
		fetchAndSet(initialParams);
	}, [fetch, fetchAndSet, initialParams]);

	return { data, refetch: fetchAndSet };
};

const useStudentDelete = () => {
	return useRequest<{ id: number }, unknown>({
		send: useCallback(
			(variables) =>
				request("student-delete.php", { method: "PUT", variables }),
			[],
		),
		update: useCallback(({ cache, variables }) => {
			return {
				...cache,
				students: cache.students.filter(
					(student) => student.id !== variables.id,
				),
			};
		}, []),
	});
};
