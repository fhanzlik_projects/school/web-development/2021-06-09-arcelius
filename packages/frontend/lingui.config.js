const { locales, defaultLocale } = require("./src/utils/locales");

module.exports = {
	locales: locales.map((locale) => locale.code),
	sourceLocale: defaultLocale.code,
	catalogs: [
		{
			path: "<rootDir>/locales/{locale}/messages",
			include: ["<rootDir>/src"],
			// exclude: ["**/node_modules/**"],
		},
	],
};
